const axios = require('axios')

const TodoApi = {
  async get () {
    return await axios.get(`${process.env.VUE_APP_API_BASE_URL}/todos`).catch((err) => {
      console.error(err)
      return err.response
    })
  },
  async add (todo) {
    return await axios.post(`${process.env.VUE_APP_API_BASE_URL}/todos`, todo).catch((err) => {
      console.error(err)
      return err.response
    })
  },
  async mark (todo) {
    return axios.patch(`${process.env.VUE_APP_API_BASE_URL}/todo/${todo.id}`, { isMarked: !todo.isMarked }).catch((err) => {
      console.error(err)
      return err.response
    })
  },
  async deleted (todo) {
    return axios.delete(`${process.env.VUE_APP_API_BASE_URL}/todo/${todo.id}`).catch((err) => {
      console.error(err)
      return err.response
    })
  }
}

module.exports = {
  TodoApi
}
