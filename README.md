# Todo App Frontend
This project contains frontend components for todo app.

| Project link:| [http://35.244.202.217](http://35.244.202.217) |
| -----------  | ----------- |

## Requirements
- [npm](https://www.npmjs.com/)

## Features
* Adding new todo to list
* Removing todo from list
* mark/unmark todo in the list

## Folder Structure

```
.
├── public                   
│   ├── index.html           
├── src           
│   ├── api            # Backend interactions are defined under this folder.
│   ├── components     # UI components are defined under this folder.
│   ├── App.vue               
│   ├── main.js
├── tests              
│   ├── pact           # Consumer driven tests are defined under this folder.
│   ├── unit           # Unit and components tests are defined under this folder
├── .gitlab-ci.yml     # Pipeline file
├── Dockerfile
├── kubernetes.yml     # Deployment and service definition are in this file. 
├── README.md
├── package.json
```
## Build and Tests
### Building the project

Project setup
```
$ npm install
```

Compiles and hot-reloads for development
```
$ npm run serve
```

Compiles and minifies for production
```
$ npm run build
```

Lints and fixes files
```
$ npm run lint
```

### Running tests 

Run your unit tests
```
$ npm run test:unit
```

Run your consumer tests
```
$ npm run test:pact:consumer
```

### Publishing Consumer Driven Contract

Publish your consumer tests
```
$ npm run test:pact:publish
```

## Deploy Project 
[Gitlab CI/CD pipeline](.gitlab-ci.yml) is defined with 4 stage. (build, test, dockerize, deploy)

## Bugs
- [ ] Pact is not working as expected in the pipeline.

## License

[GNU GENERAL PUBLIC LICENSE](LICENSE)