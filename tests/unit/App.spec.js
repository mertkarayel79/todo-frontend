import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import App from '@/App.vue'
import Todo from '@/components/Todo.vue'

describe('App.vue', () => {
  describe('initial rendering', () => {
    it('should include Todo component', () => {
      const appWrapper = mount(App)
      expect(appWrapper.contains(Todo)).to.equal(true)
    })
  })
})
