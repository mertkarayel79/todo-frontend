import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import TodoAdd from '@/components/TodoAdd.vue'

describe('TodoAdd.vue', () => {
  describe('initial rendering', () => {
    it('should include input text with default placeholder and button with named as Add', () => {
      const todoAddWrapper = mount(TodoAdd)
      const defaultPlaceholder = 'What do you want to do ?'
      expect(todoAddWrapper.contains('.new-todo')).to.equal(true)
      expect(todoAddWrapper.find('.new-todo').attributes().placeholder).to.equal(defaultPlaceholder)
      expect(todoAddWrapper.contains('.add-button')).to.equal(true)
      expect(todoAddWrapper.get('.add-button').text()).to.include('Add')
    })
    it('should change placeholder when custom placeholder is passed.', () => {
      const placeholder = 'Custom Placeholder'
      const todoAddWrapper = mount(TodoAdd, {
        propsData: {
          placeholder
        }
      })
      expect(todoAddWrapper.props().placeholder).to.equal(placeholder)
      expect(todoAddWrapper.find('.new-todo').attributes().placeholder).to.equal(placeholder)
    })
  })
  describe('add button was clicked', () => {
    it('with empty text and it should not emit text to add', () => {
      const todoAddWrapper = mount(TodoAdd)
      todoAddWrapper.get('.add-button').trigger('click')
      todoAddWrapper.vm.add()
      expect(todoAddWrapper.emitted('add')).to.equal(undefined)
    })
    it('with full text and it should emit text to add', () => {
      const test = 'test'
      const todoAddWrapper = mount(TodoAdd, {
        data: function () {
          return {
            text: test
          }
        }
      })
      todoAddWrapper.get('.add-button').trigger('click')
      expect(todoAddWrapper.emitted('add')[0]).to.be.deep.equal([{ text: test }])
    })
  })
})
