import { expect } from 'chai'
import { mount } from '@vue/test-utils'
import TodoItem from '@/components/TodoItem.vue'

describe('TodoItem.vue', () => {
  describe('initial rendering', () => {
    it('should include .todo-item selector', () => {
      const todoItemWrapper = mount(TodoItem, {
        propsData: {
          todo: { message: 'test' }
        }
      })
      expect(todoItemWrapper.contains('.todo-item')).to.equal(true)
    })
  })
  describe('item label text was clicked', () => {
    it('with full text and it should emit todo to marked', () => {
      const todo = { text: 'test', isMarked: false }
      const todoItemWrapper = mount(TodoItem, {
        propsData: {
          todo: todo
        }
      })

      todoItemWrapper.get('.todo').trigger('click')
      expect(todoItemWrapper.emitted('marked')[0]).to.be.deep.equal([todo])
    })
  })
  describe('todo marked as false', () => {
    it('and it should not add css to todo label', () => {
      const todo = { text: 'test', isMarked: false }
      const todoItemWrapper = mount(TodoItem, {
        propsData: {
          todo: todo
        }
      })
      const style = todoItemWrapper.find('.todo').element.style
      expect(style.length).to.equal(0)
    })
  })
  describe('todo marked as false', () => {
    it('and it should add `text-decoration` css to todo label', () => {
      const todo = { text: 'test', isMarked: true }
      const todoItemWrapper = mount(TodoItem, {
        propsData: {
          todo: todo
        }
      })
      const style = todoItemWrapper.find('.todo').element.style
      expect(style[0]).to.equal('text-decoration')
    })
  })
  describe('delete button text was clicked', () => {
    it('and it should emit todo to deleted', () => {
      const todo = { text: 'test', isMarked: false }
      const todoItemWrapper = mount(TodoItem, {
        propsData: {
          todo: todo
        }
      })

      todoItemWrapper.get('.delete-button').trigger('click')
      expect(todoItemWrapper.emitted('deleted')[0]).to.be.deep.equal([todo])
    })
  })
})
