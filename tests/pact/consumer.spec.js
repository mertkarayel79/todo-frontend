const chai = require('chai')
const { provider } = require('./pact')
const chaiAsPromised = require('chai-as-promised')
const { TodoApi } = require('../../src/api/TodoApi')
const { like } = require('@pact-foundation/pact').Matchers

const expect = chai.expect
chai.use(chaiAsPromised)

describe('Pact with Todo API', () => {
  before(() =>
    provider.setup().then(opts => {
      process.env.VUE_APP_API_BASE_URL = `http://localhost:${opts.port}`
    })
  )

  afterEach(() => provider.verify())

  const id = 1
  const unknownId = 100
  const markedRequest = { isMarked: true }
  const unMarkedRequest = { isMarked: false }
  const todoPayloadCheck = function (expected, actual) {
    expect(expected.id).to.equal(actual.id)
    expect(expected.text).to.equal(actual.text)
    expect(expected.isMarked).to.equal(actual.isMarked)
  }

  describe('call todo service', () => {
    describe('for getting todos', () => {
      describe('when list is empty ', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'get todos when list is empty',
            withRequest: {
              path: '/todos',
              method: 'GET'
            },
            willRespondWith: {
              body: like([]),
              status: 200,
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              }
            }
          })
        })

        it('will receive empty todo list response', async () => {
          const response = await TodoApi.get()
          return expect(response.data.length).equal([].length)
        })
      })
      describe('when list is full ', () => {
        const expectedResponse = [
            {
              id: 1,
              text: 'test',
              isMarked: false
            },
            {
              id: 2,
              text: 'test2',
              isMarked: false
            }
          ]

        before(() => {
          return provider.addInteraction({
            state: 'todo list',
            uponReceiving: 'get todos when list is full',
            withRequest: {
              path: '/todos',
              method: 'GET'
            },
            willRespondWith: {
              body: like(expectedResponse),
              status: 200,
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              }
            }
          })
        })

        it('will receive list of todos response', async () => {
          const response = await TodoApi.get()
          expect(response.data.length).to.equal(expectedResponse.length)
          todoPayloadCheck(response.data[0], expectedResponse[0])
          todoPayloadCheck(response.data[1], expectedResponse[1])
        })
      })
    })

    describe('for creating new todo', () => {
      describe('with todo\'s text as ""', () => {
        const request = {
          text: ''
        }

        before(() => {
          return provider.addInteraction({
            uponReceiving: 'create new todo with invalid text',
            withRequest: {
              path: '/todos',
              method: 'POST',
              body: like(request)
            },
            willRespondWith: {
              status: 400,
            }
          })
        })

        it('will receive error response', async () => {
          const response = await TodoApi.add(request)
          expect(response.status).to.equal(400)
        })
      })
      describe('with todo\'s text as "test"', () => {
        const request = {
          text: 'test'
        }

        const expectedResponse = {
          id: 1,
          text: 'test',
          isMarked: false
        }

        before(() => {
          return provider.addInteraction({
            uponReceiving: 'create new todo with valid text value',
            withRequest: {
              path: '/todos',
              method: 'POST',
              body: like(request)
            },
            willRespondWith: {
              body: like(expectedResponse),
              status: 201,
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              }
            }
          })
        })

        it('will receive created todo response', async () => {
          const response = await TodoApi.add(request)
          todoPayloadCheck(expectedResponse, response.data)
        })
      })
    })

    describe('for marking todo', () => {
      describe('when unknown todo id', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'mark unknown todo id',
            withRequest: {
              path: `/todo/${unknownId}`,
              method: 'PATCH',
              body: like(markedRequest)
            },
            willRespondWith: {
              status: 404,
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: unknownId, isMarked: markedRequest.isMarked}
          const response = await TodoApi.mark(todo)
          expect(response.status).to.equal(404)
        })
      })
      describe('when known todo id', () => {

        before(() => {
          return provider.addInteraction({
            uponReceiving: 'mark known todo id',
            state: 'unmarked todo',
            withRequest: {
              path: `/todo/${id}`,
              method: 'PATCH',
              body: like(markedRequest)
            },
            willRespondWith: {
              status: 200,
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: id, isMarked: markedRequest.isMarked}
          const response = await TodoApi.mark(todo)
          expect(response.status).to.equal(200)
        })
      })
    })

    describe('for un-marking todo', () => {
      describe('when unknown todo id', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'unmark unknown todo id',
            withRequest: {
              path: `/todo/${unknownId}`,
              method: 'PATCH',
              body: like(unMarkedRequest)
            },
            willRespondWith: {
              status: 404,
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: unknownId, isMarked: markedRequest.isMarked}
          const response = await TodoApi.mark(todo)
          expect(response.status).to.equal(404)
        })
      })
      describe('when known todo id', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'unmark known todo id',
            state: 'marked todo',
            withRequest: {
              path: `/todo/${id}`,
              method: 'PATCH',
              body: like(unMarkedRequest)
            },
            willRespondWith: {
              status: 200
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: id, isMarked: unMarkedRequest.isMarked}
          const response = await TodoApi.mark(todo)
          expect(response.status).to.equal(200)
        })
      })
    })

    describe('for deleting todo id', () => {
      describe('when unknown todo id', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'delete unknown todo id',
            withRequest: {
              path: `/todo/${unknownId}`,
              method: 'DELETE'
            },
            willRespondWith: {
              status: 404,
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: unknownId, text: "text", isMarked: false}
          const response = await TodoApi.deleted(todo)
          expect(response.status).to.equal(404)
        })
      })
      describe('when known todo id', () => {
        before(() => {
          return provider.addInteraction({
            uponReceiving: 'delete known todo id',
            state: 'deletable todo',
            withRequest: {
              path: `/todo/${id}`,
              method: 'DELETE'
            },
            willRespondWith: {
              status: 204,
            }
          })
        })

        it('will receive error response', async () => {
          const todo = {id: id, text: "text", isMarked: false}
          const response = await TodoApi.deleted(todo)
          expect(response.status).to.equal(204)
        })
      })
    })
  })

  // Write pact files to file
  after(() => {
    return provider.finalize()
  })
})
