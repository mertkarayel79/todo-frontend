const pact = require('@pact-foundation/pact')
const Pact = pact.Pact
const path = require('path')
const consumerName = 'TodoWeb'
const providerName = 'TodoApi'
const pactFile = path.resolve(
  `./pacts/${consumerName}-${providerName}.json`.toLowerCase()
)

module.exports = {
  pactFile
}

const provider = new Pact({
  log: path.resolve(__dirname, 'logs', 'pact.log'),
  dir: path.resolve(__dirname, 'pacts'),
  logLevel: 'FATAL',
  host: '0.0.0.0',
  consumer: consumerName,
  provider: providerName
})

// used to kill any left over mock server instances in case of errors
process.on('SIGINT', () => {
  pact.removeAllServers()
})

module.exports = {
  provider,
  pactFile,
  consumerName,
  providerName,
  consumerVersion: '1.0.0'
}
