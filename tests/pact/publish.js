const pact = require('@pact-foundation/pact-node')
const path = require('path')
const opts = {
  pactFilesOrDirs: [path.resolve(__dirname, 'pacts')],
  pactBroker: 'https://karayel.pact.dius.com.au/',
  pactBrokerToken: 'rmfU9J6Ywu81jboafLJvHw',
  tags: ['prod', 'test'],
  consumerVersion: '1.0.1'
}

pact
  .publishPacts(opts)
  .then(() => {
    console.log('Pact contract publishing complete!')
  })
  .catch(e => {
    console.log('Pact contract publishing failed: ', e)
  })
